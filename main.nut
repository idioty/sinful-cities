
import("util.FGVersionController", "FGVersionController", 2);
import("util.superlib", "SuperLib", 36);

require("FKUtils.nut");
require("FKDate.nut");
require("FKList.nut");
require("FKTile.nut");
require("FKRailTile.nut");
require("FKTown.nut");

class Unemployment extends GSController {
	// feladat tipusok elraktarozva ertelmes nevvel

	firstcomp = null; // proba bellaitas...

	static TOWN_GROWTH_NONE = 0xFFFF;
	static TOWN_GROWTH_NORMAL = 0x10000;

	loaded_game = false;	// uj jatek, vagy betoltes
	back_to_normal = false; // ez a valtozo csak akkor kell, ha kikapcsoljuk a jatekot


	utils = null;
	railtile = null;
	date = null;


	versionController = null;
	companies = null;		// ebben tarolom a vallalatokat, hogy kinek milyen volt az elmult eve
	companyNames = null;
	preferences = null;		// beallitasokat tartalmazo fajl
	stopped_towns = null;	// ebben tarolom, hogy melyk varosoknak allitottam meg a novekedeset


	startyear = 0;
	constructor() {
		this.utils = FKUtils(0);
		this.railtile = FKRailTile(this.utils);
		this.date = FKDate();

		this.versionController = FGVersionController(1, 2, 0);
		this.companies = [];
		this.companyNames = [];
		this.stopped_towns = [];
		this.preferences = {
			effect_on = 3,			// ertekei: 1-3		melyik atlag varos ratingnel induljon be a mukodes
			abs_month_rate = 6,		// ertekei: 3,6,12	hany honap atlagat szamoljuk, alap a 6
			month_activate = 3,		// ertekei: 3,6,12	hany havonta bunozzenek
		}
	}
}

function Unemployment::Start() {
	/* Wait for the game to start */
	this.Sleep(1);

	this.utils.Log("[Unemployment::Start] Start", FKUtils.LOGLEVEL_ALL);

	if (this.loaded_game && GSGame.IsPaused()) {
		GSGame.Unpause();
	}


	if (!this.loaded_game) {
		this.LoadPreferences();
		this.date.startyear = GSDate.GetYear(GSDate.GetCurrentDate());
	}
	else {
		this.utils.Log("[Unemployment::Start] Loaded saved game...", FKUtils.LOGLEVEL_ALL);
	}
	
	while (true) {
		local ticks = GSController.GetTick();

		if (GSController.GetSetting("enabled") != -1 && GSController.GetSetting("enabled")) {
			this.back_to_normal = false;
			this.HandleEvents();
			this.UpdateCompanyNames();
			
//			local days = this.GetCurrentDaysCount();
			local actday = FKDate.GetCurrentDay();

			// updateljuk a rail queue-t
			this.railtile.QueueUpdate();

			if (actday == 1) // ugyis csak havonta egyszer valtozik a vallalatok megitelese
				this.Update();


			if (false && (actday % 5) == 0) {
				if (false) {
					local t1 = GSMap.GetTileIndex(66, 76);
					local t2 = GSMap.GetTileIndex(68, 76);

					if (GSRail.IsRailTile(t1) && GSRail.IsRailTile(t2)) {
						GSLog.Info("");
						local at = GSController.GetTick();
						GSLog.Warning("isTileConnected: " + (this.railtile.IsRailTileConnectedToTile(t1, t2) ? "YES" : "NO"));
						local at2 = GSController.GetTick();
						GSLog.Info("ticks: " + (at2 - at).tostring());
						GSLog.Info("");
					}
				}

				local towns = GSTownList();
				GSLog.Info("--------");
				GSLog.Info("");

				foreach (town_id, _ in towns) {
					local rsl = this.railtile.GetAllRailStationFromTown(firstcomp, town_id)
					if (rsl == null || rsl.Count() == 0) continue;

					local first_station = rsl.Begin();

					local i = 0;
					local tiles = GSTileList_StationType(first_station, GSStation.STATION_TRAIN);
					foreach (tile_id, _ in tiles) {
						GSLog.Info("Station tile " + i + ": " + GSMap.GetTileX(tile_id) + " | " + GSMap.GetTileY(tile_id));
						i++;
					}

					GSLog.Info("");


					i = 0;
					local rtiles = this.railtile.GetAllRailTileAroundStationMaxDistance(first_station, 3);
					if (rtiles != null) {
						foreach (tile_id, _ in rtiles) {
							GSLog.Info("Rail tile " + i + ": " + GSMap.GetTileX(tile_id) + " | " + GSMap.GetTileY(tile_id));
							i++;
						}

						GSLog.Info("");
						break;
					}
				}
			}
		}
		else {
			if (!this.back_to_normal) {
				// minden varos novekedeset alap helyzetbe allitjuk
				this.back_to_normal = true;
				foreach (town_id, _ in GSTownList()) {
					if (!GSTown.SetGrowthRate(town_id, this.TOWN_GROWTH_NORMAL))
						this.utils.Log("[Unemployment::Start] Can not set NORMAL GrowthRate for town: " + GSTown.GetName(town_id), FKUtils.LOGLEVEL_ERRORS_AND_WARNINGS);
				}
			}
		}

		// Loop with a frequency of five days
		local ticks_used = GSController.GetTick() - ticks;
		local day = 1;
		local tick = (day * 74) - ticks_used;
		if (tick < 1)
			tick = 1;
		this.Sleep(tick);
	}
}

function Unemployment::LoadPreferences() {
	this.utils.Log("[Unemployment::LoadPreferences] Loading preferences...", FKUtils.LOGLEVEL_ALL);
	// TODO: ezt se felejtsd el
	// possible betoltes
//	if ((GSController.GetSetting("shouldRailDeposMore") != -1) && (GSController.GetSetting("shouldRailDeposNum") != -1))
//		if (!GSController.GetSetting("shouldRailDeposMore"))


	/*
	if (this.testenabled) {
		this.utils.Log("[FecaGame::LoadPreferences] Proba beallitasokat ki kell kapcsolni.", FKUtils.LOGLEVEL_ERRORS_AND_WARNINGS);
	}
	 */
}

function Unemployment::HandleEvents() {
	if (GSEventController.IsEventWaiting()) {
		local ev = GSEventController.GetNextEvent();
		
		switch (ev.GetEventType()) {
			case GSEvent.ET_COMPANY_NEW:
				local instance = GSEventCompanyNew.Convert(ev);
				local company_id = instance.GetCompanyID();
				if (company_id == GSCompany.COMPANY_INVALID) break;

				if (firstcomp == null)
					firstcomp = company_id;

				if (!this.versionController.IsSupportedVersion()) {
					// kuldunk uzenetet a vallalatoknak
					GSGoal.Question(0, company_id, GSText(GSText.STR_NOT_COMPATIBLE), GSGoal.QT_INFORMATION, GSGoal.BUTTON_CLOSE);
					break;
				}

				/*
				if (!this.versionController.IsSupportedVersion()) {
					// kuldunk uzenetet a vallalatoknak
					GSGoal.Question(0, company_id, GSText(GSText.STR_NOT_COMPATIBLE), GSGoal.QT_INFORMATION, GSGoal.BUTTON_CLOSE);
					break;
				}
				 */
				
				local compName = GSCompany.GetName(company_id);
				this.utils.Log("[Unemployment::HandleEvents] New company: " + compName, FKUtils.LOGLEVEL_ALL);
				
				this.AddCompany(company_id, compName);
				break;
				
			case GSEvent.ET_COMPANY_BANKRUPT:
				local instance = GSEventCompanyBankrupt.Convert(ev);
				local company_id = instance.GetCompanyID();
				if (company_id == GSCompany.COMPANY_INVALID) break;
//				local company = this.GetCompany(company_id);
//				if (company == null) break;

//				local companyScores = company.GetScores();
				local compName = this.CompanyName(company_id);
				
				this.RemoveCompany(company_id);
				break;
				
				
			case GSEvent.ET_COMPANY_MERGER:
				local instance = GSEventCompanyMerger.Convert(ev);
				local company_id = instance.GetOldCompanyID();
				if (company_id == GSCompany.COMPANY_INVALID) break;
//				local company = this.GetCompany(company_id);
//				if (company == null) break;

				
				local new_company_id = instance.GetNewCompanyID();
				local newCompName = this.CompanyName(new_company_id);
				
//				local companyScores = company.GetScores();
				local oldCompName = this.CompanyName(company_id);
				
				this.RemoveCompany(company_id);
				break;
		}
	}
}

function Unemployment::Update() {
	local towns = GSTownList();

	foreach (town_id, _ in towns) {
		local hascompany = false;
		local town_rate = 0;
		local town_rate_count = 0;

		foreach (company in this.companies) {
			local rating = GSTown.GetRating(town_id, company.company_id);
			if (rating == GSTown.TOWN_RATING_NONE || rating == GSTown.TOWN_RATING_INVALID) {
				// eltavolitjuk a varost, ha hozza van adva a companyhoz
				for (local i = 0; i < company.towns.len(); i++) {
					if (company.towns[i].town_id == town_id) {
						company.towns.remove(i);
						break;
					}
				}
			}
			else {
				// megcsinaljuk az ellenorzest, es elmentjuk, hogy legutobb mi volt
				hascompany = true;
				town_rate += FKTown.GetValueOfTownRating(rating);
				town_rate_count++;

				this.AddCompanyRatingToTown(company, town_id, rating);
			}
		}

		if (hascompany) {
			local now_month = GSDate.GetMonth(GSDate.GetCurrentDate());
			local kell = ((now_month % this.preferences.month_activate) == 0);

			local newrate = FKTown.GetTownRatingFromValue(town_rate / town_rate_count); // elvileg nem lehet 0 a town_rate_count
			if (newrate > 0 && newrate <= this.preferences.effect_on && kell) {
				// tehat ellenorizni kell, hogy allitani kell-e a novekedesen

				// tehat nincs meg a megfelelo elegedettsegi szint
				// leallitjuk a novekedest
				if (GSTown.SetGrowthRate(town_id, this.TOWN_GROWTH_NONE)) {
					if (!FKUtils.ValueIsInArray(town_id, this.stopped_towns))
						this.stopped_towns.append(town_id);
				}
				else {
					this.utils.Log("[Unemployment::Update] Can not set NONE GrowthRate for town: " + GSTown.GetName(town_id), FKUtils.LOGLEVEL_ERRORS_AND_WARNINGS);
				}

				// Johet a lenyeg
//				this.utils.Log("Elvileg rossz a megitelese osszessegeben: " + GSTown.GetName(town_id), FKUtils.LOGLEVEL_ALL);
				// TODO: nem csak sin es utrombolassal lehetne akadalyozni,
				// hanem a varosban kozlekedo eszkoz rakomanyanak lopasaval is lehetne.
				// Ha utast rabolnak ki, akkor penzbirsag, ha rakomanyt, akkor meg szerzodes szeges miatt penz levonas a jutalekbol
				// Ezen kivul lehetne olyat, hogy megallitanak buszt vagy vonatot es "kiraboljak", ekkor megall a vonat/busz is. Vagy ha lehet depoba kuldeni, akkor meg alkatresz lopas miatt, pl repulo, hajo
				// utlopas nem jo otlet, mert nem biztos, hogy azzal akadalyozva lennenek
				// robbantas is szoba johetne
				// sztrajkok
				// jelzolampa megfroditasa / egyiranyusitasa

				// megkeressuk, hogy melyik company az akitol lopni kell
				foreach (company in this.companies) {
					local rating = (this.GetCompanyRatingOfTown(company, town_id)).tointeger();
					if (rating > 0 && rating <= this.preferences.effect_on) {
						this.utils.Log("[Unemployment::Update] The people are malcontent in town: " + GSTown.GetName(town_id) + "with company: " + GSCompany.GetName(company.company_id) + ", therefore the crime rate is increased.", FKUtils.LOGLEVEL_ALL);

						local tile = this.railtile.GetRandomRailTileInTown(company.company_id, town_id);
						if (tile != null) {
//							if (FKTile.DemolishTile(company.company_id, tile)) {
//							if (this.railtile.RemoveRailsOnTile(company.company_id, tile, true)) {
							if (this.railtile.QueueRemoveRailsOnTile(company.company_id, tile, true, {town_id = town_id}, this)) {
								this.utils.Log("[Unemployment::Update] Demolish tile", FKUtils.LOGLEVEL_ALL);
								local message = "Tolvajok okoztak kárt " + GSCompany.GetName(company.company_id) + " vállalatnak. Síneket rongáltak meg " + GSTown.GetName(town_id) + " közelében. Valószínűleg a vállalat rossz megítélése miatt.";
								GSNews.Create(GSNews.NT_GENERAL, message, GSCompany.COMPANY_INVALID);
							}
							else {
								// TODO: akkor sem sikerul lerombolni sint, ha nincs penze az uriembernek...
								this.utils.Log("[Unemployment::Update] Can not demolish tile", FKUtils.LOGLEVEL_ERRORS_AND_WARNINGS);
							}
						}
					}
				}
			}
			else {
				for (local i = 0; i < this.stopped_towns.len(); i++) {
					if (this.stopped_towns[i] == town_id) {
						// visszaallitjuk az alapba, mert lehet megszunt a bunozes
						this.utils.Log("Visszaallitjuk a varost, mert megszunt a bunozes: " + GSTown.GetName(town_id), FKUtils.LOGLEVEL_ALL);
						if (!GSTown.SetGrowthRate(town_id, this.TOWN_GROWTH_NORMAL))
							this.utils.Log("[Unemployment::Update] Can not set NORMAL GrowthRate for town: " + GSTown.GetName(town_id), FKUtils.LOGLEVEL_ERRORS_AND_WARNINGS);

						this.stopped_towns.remove(i);
						break;
					}
				}
			}
		}
		else {
			if (!GSTown.SetGrowthRate(town_id, this.TOWN_GROWTH_NORMAL))
				this.utils.Log("[Unemployment::Update] Can not set NORMAL GrowthRate for town: " + GSTown.GetName(town_id), FKUtils.LOGLEVEL_ERRORS_AND_WARNINGS);

			for (local i = 0; i < this.stopped_towns.len(); i++) {
				if (this.stopped_towns[i] == town_id) {
					this.stopped_towns.remove(i);
					break;
				}
			}
		}
	}
}

function Unemployment::RailTileQueueSucces(company_id, otherinformations) {
	GSLog.Warning("  ******** SIKERESEN TOROLTUK KESOBB ********  ");
	// TODO: ezt meg be kell fejezni
}

// - # Company functions
function Unemployment::UpdateCompanyNames() {
	// frissitjuk mindig a vallalatok nevet:
	foreach (compinfo in this.companyNames) {
		if (GSCompany.ResolveCompanyID(compinfo.company_id) == GSCompany.COMPANY_INVALID) continue;
		local newName = GSCompany.GetName(compinfo.company_id);
		if (newName != null && newName != compinfo.company_name) {
			this.utils.Log("[Unemployment::UpdateCompanyNames] " + compinfo.company_name + " company renamed to: " + newName, FKUtils.LOGLEVEL_ALL);

			compinfo.company_name = newName;
		}
	}
}

function Unemployment::RemoveCompany(a_company_id) {
	for (local i = 0; i < this.companies.len(); i++) {
		if (this.companies[i].company_id == a_company_id) {
			this.companies.remove(i);
			break;
		}
	}

	for (local i = 0; i < this.companyNames.len(); i++) {
		if (this.companyNames[i].company_id == a_company_id) {
			this.companyNames.remove(i);
			break;
		}
	}
}

function Unemployment::AddCompany(a_company_id, companyName) {
	local compdata = {
		company_id = a_company_id,
		towns = [],
	}

	this.companies.append(compdata);
	this.AddToCompanyNames(a_company_id, companyName);
}

function Unemployment::AddToCompanyNames(company_id, companyName) {
	this.companyNames.append({company_id = company_id, company_name = companyName});
}

function Unemployment::CompanyName(company_id) {
	for (local i = 0; i < this.companyNames.len(); i++) {
		if (this.companyNames[i].company_id == company_id)
			return this.companyNames[i].company_name;
	}
	
	return null;
}

function Unemployment::AddCompanyRatingToTown(company, a_town_id, rating) {
	local van = false;
	local current_month = this.date.GetMonthsCountFromStarted();

	for (local i = 0; i < company.towns.len(); i++) {
		local town = company.towns[i];

		if (town.town_id == a_town_id) {
			town.town_ratings.append(rating);

			// mivel ugyis csak max a beallitott mennyisegu honapra visszamenoleg ellenorzunk, ezert felesleges a tobbi adat
			while (town.town_ratings.len() > this.preferences.abs_month_rate) {
				town.town_ratings.remove(0);
			}

			van = true;
			break;
		}
	}

	if (!van) {
		local townrating = [];
		townrating.append(rating);
		company.towns.append({town_id = a_town_id, town_ratings = townrating});
	}
}

function Unemployment::GetCompanyRatingOfTown(company, town_id) {
	local counter = 0;
	local rates = 0;

	for (local i = 0; i < company.towns.len(); i++) {
		local town = company.towns[i];

		if (town.town_id == town_id) {
			foreach (rate in town.town_ratings) {
				if (rate > 0) { // ahol 0, ott vagy nincs jelen, vagy invalid erteku, azert nem szamoljuk...
					rates += rate;
					counter++;
				}
			}

			break;
		}
	}

	local result = 0;
	if (counter > 0)
		result = rates / counter;

	return FKTown.GetTownRatingFromValue(result);
}

// TODO: FKRailTile mentese, betoltese
function Unemployment::Save() {
	this.utils.Log("[Unemployment::Save] Saving game...", FKUtils.LOGLEVEL_ALL);
	
	
	return {
		loglevel = this.utils.loglevel,
		back_to_normal = this.back_to_normal,
		companies = this.companies,
		companyNames = this.companyNames,
		preferences = this.preferences,
		stopped_towns = this.stopped_towns,
		startyear = this.date.startyear,
	};
}

function Unemployment::Load(version, table) {
	this.utils.Log("[Unemployment::Load] Loading game...", FKUtils.LOGLEVEL_ALL);
	
	this.loaded_game = true;

	// Store a copy of the table from the save game
	// but do not process the loaded data yet. Wait with that to Init
	// so that OpenTTD doesn't kick us for taking too long to load.	
	local loaded_data = {}
   	foreach(key, value in table)
		loaded_data.rawset(key, value);

	this.utils.loglevel = loaded_data.loglevel;
	this.back_to_normal = loaded_data.back_to_normal;
	this.companies = loaded_data.companies;
	this.companyNames = loaded_data.companyNames;
	this.preferences = loaded_data.preferences;
	this.stopped_towns = loaded_data.stopped_towns;
	this.date.startyear = loaded_data.startyear;
}
