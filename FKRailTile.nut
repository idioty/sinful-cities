
class FKRailTile {
	static RAIL_DIRECTION_NE = 0;
	static RAIL_DIRECTION_SE = 1;
	static RAIL_DIRECTION_NW = 2;
	static RAIL_DIRECTION_SW = 3;


	static RANDOM_RAIL_TILE_DISTANCE_FROM_STATION = 20;

	utils = null;
	queue = null;

	constructor (fkutils) {
		this.utils = fkutils;
		this.queue = [];
	}
}

function FKRailTile::SaveToData() {
	return {
		queue = this.queue,
	}
}

function FKRailTile::LoadFromData(data) {
	this.queue.clear();
	this.queue.extend(data.queue);
}

// # - Karokozo funkciok
function FKRailTile::GetRandomRailTileInTown(company_id, town_id) {
	// TODO: nem station alapjan kellene csinalni, mert ha olyan station-t valaszt ki amihez kozvetlenul nem kapcsolodik sin, akkor semmit nem csinal
	// tehat a varos teruletet kellene megkeresni es ugy es annak a teruleten robbantani sint...

	local companymode = GSCompanyMode(FKUtils.GetCompanyMode(company_id));
	local stations = GSStationList(GSStation.STATION_TRAIN);

	if (stations.Count() == 0) {
		this.utils.Log("[FKRailTile::GetRandomRailTileInTown] " + GSCompany.GetName(company_id) + " has no railstations", FKUtils.LOGLEVEL_ERRORS_AND_WARNINGS);
		return null;
	}
	else {
		local town_stations = [];

		foreach (station_id, _ in stations) {
			if (GSStation.GetNearestTown(station_id) == town_id)
				town_stations.append(station_id);
		}

		if (town_stations.len() == 0) {
			this.utils.Log("[FKRailTile::GetRandomRailTileInTown] " + GSCompany.GetName(company_id) + " has no railstations in town: " + GSTown.GetName(town_id), FKUtils.LOGLEVEL_ERRORS_AND_WARNINGS);
			return null;
		}
		else {
			local random_station_id = town_stations[GSBase.RandRange(town_stations.len())];
			local randomTiles = this.GetAllRailTileAroundStationMaxDistance(random_station_id, this.RANDOM_RAIL_TILE_DISTANCE_FROM_STATION);
			if (randomTiles == null)
				return null;

			local tiles = [];
			foreach (tile_id, _ in randomTiles) {
				tiles.append(tile_id);
			}

			return tiles[GSBase.RandRange(tiles.len())];
		}
	}

	return null;
}

function FKRailTile::IsNieighbourConnected(tile_id, n_tile_id) {
	assert(GSMap.IsValidTile(tile_id));
	assert(GSMap.IsValidTile(n_tile_id));
	assert(tile_id != n_tile_id);
	assert(GSMap.DistanceManhattan(tile_id, n_tile_id) == 1);

	local ax = GSMap.GetTileX(tile_id);
	local ay = GSMap.GetTileY(tile_id);
	local nx = GSMap.GetTileX(n_tile_id);
	local ny = GSMap.GetTileY(n_tile_id);

	local adirection = null;
	local ndirection = null;

	if (ax < nx) {
		adirection = this.RAIL_DIRECTION_SW;
		ndirection = this.RAIL_DIRECTION_NE;
	}
	else if (ax > nx) {
		adirection = this.RAIL_DIRECTION_NE;
		ndirection = this.RAIL_DIRECTION_SW;
	}
	else if (ay < ny) {
		adirection = this.RAIL_DIRECTION_SE;
		ndirection = this.RAIL_DIRECTION_NW;
	}
	else if (ay > ny) {
		adirection = this.RAIL_DIRECTION_NW;
		ndirection = this.RAIL_DIRECTION_SE;
	}

	if (adirection == null || ndirection == null)
		return false;

	//	GSLog.Info("adir: " + adirection + ", x: " + ax + ", y: " + ay);
	//	GSLog.Info("ndir: " + ndirection + ", x: " + nx + ", y: " + ny);

	/* TODO: jelenleg 90 fokot is tehet az ellenorzes, meg kell oldani, hogy ilyen ne legyen
	 local forbid90 = true;
	 if (GSGameSettings.IsValid("pf.forbid_90_deg") && !GSGameSettings.GetValue("pf.forbid_90_deg"))
	 forbid90 = false;
	 */

	// TODO: van meg egy olyan hiba is, hogy ha egy mezon ket parhuzamos sin fut, akkor atter a masikra is a kereses,
	// hiaba nem kapcsolodik az elozo hozza

	return (this.IsTileCanConnectToDirection(adirection, tile_id) &&
	this.IsTileCanConnectToDirection(ndirection, n_tile_id));
}

function FKRailTile::IsTileCanConnectToDirection(direction, tile_id) {
	assert(direction != GSRail.RAILTRACK_INVALID);
	assert(GSMap.IsValidTile(tile_id));

	local bitmap = GSRail.GetRailTracks(tile_id);
	if (bitmap == null || bitmap == GSRail.RAILTRACK_INVALID || bitmap == 0x00)
		return false;

	local van = false;

	switch (direction) {
		case this.RAIL_DIRECTION_NE:
			if (bitmap & GSRail.RAILTRACK_NW_NE)
				van = true;
			else if (bitmap & GSRail.RAILTRACK_NE_SW)
				van = true;
			else if (bitmap & GSRail.RAILTRACK_NE_SE)
				van = true;
			break;
		case this.RAIL_DIRECTION_SE:
			if (bitmap & GSRail.RAILTRACK_NE_SE)
				van = true;
			else if (bitmap & GSRail.RAILTRACK_NW_SE)
				van = true;
			else if (bitmap & GSRail.RAILTRACK_SW_SE)
				van = true;
			break;
		case this.RAIL_DIRECTION_NW:
			if (bitmap & GSRail.RAILTRACK_NW_NE)
				van = true;
			else if (bitmap & GSRail.RAILTRACK_NW_SE)
				van = true;
			else if (bitmap & GSRail.RAILTRACK_NW_SW)
				van = true;
			break;
		case this.RAIL_DIRECTION_SW:
			if (bitmap & GSRail.RAILTRACK_NW_SW)
				van = true;
			else if (bitmap & GSRail.RAILTRACK_NE_SW)
				van = true;
			else if (bitmap & GSRail.RAILTRACK_SW_SE)
				van = true;
			break;
	}
	
	return van;
}


function FKRailTile::GetAllRailStationFromTown(company_id, town_id) {
	assert(company_id != null);
	assert(town_id != null);
	assert(GSTown.IsValidTown(town_id));

	local companymode = GSCompanyMode(FKUtils.GetCompanyMode(company_id));
	local stations = GSStationList(GSStation.STATION_TRAIN);

	if (stations.Count() == 0) {
		this.utils.Log("[FKRailTile::GetAllRailStationFromTown] " + GSCompany.GetName(company_id) + " Has no railstations in town: " + GSTown.GetName(town_id), FKUtils.LOGLEVEL_ERRORS_AND_WARNINGS);
		return null;
	}
	else {
		local town_stations = GSList();

		foreach (station_id, _ in stations) {
			if (GSStation.GetNearestTown(station_id) == town_id && (GSTileList_StationType(station_id, GSStation.STATION_TRAIN).Count() > 0))
				town_stations.AddItem(town_stations.Count(), station_id);
		}

		return town_stations;
	}

	return null;
}

function FKRailTile::GetAllRailTileAroundStationMaxDistance(station_id, distance) {
	assert(station_id != null);
	assert(GSStation.IsValidStation(station_id));
	assert(distance > 0);

	local company_id = GSStation.GetOwner(station_id);
	local companymode = GSCompanyMode(FKUtils.GetCompanyMode(company_id));

	local stationTiles = GSTileList_StationType(station_id, GSStation.STATION_TRAIN);

	if (stationTiles.Count() == 0) {
		this.utils.Log("[FKRailTile::GetRandomRailTileAroundStationMaxDistance] Station has no railstation tile", FKUtils.LOGLEVEL_ERRORS_AND_WARNINGS);
		return null;
	}

	local railtiles = [];
	foreach (tile_id, _ in stationTiles) {
		local tiles = FKTile.GetNeighbours(tile_id);

		foreach (a_tile_id, _ in tiles) {
			if (FKUtils.ValueIsInArray(a_tile_id, railtiles) ||					// ha kozte van mar
			GSTile.IsStationTile(a_tile_id) ||								// ha station tile
			(GSTile.GetOwner(a_tile_id) != company_id) ||					// ha nem current company az owner
			!GSTile.HasTransportType(a_tile_id, GSTile.TRANSPORT_RAIL))		// es ha egyaltalan nem rail tilerol van-e szo
			continue;															// akkor folytatjuk a keresest

			if (this.IsNieighbourConnected(tile_id, a_tile_id)) {
				railtiles.append(a_tile_id);
			}
		}
	}

	// ilyenkor elvileg megvan az osszes allomas koruli sajat vasuti sin
	if (railtiles.len() == 0) {
		// TODO: talan azert is elofordulhat ilyen, mert nincs sin az allomas melle epitve, ezert mas modon kell bosszut allni
		this.utils.Log("[FKRailTile::GetRandomRailTileAroundStationMaxDistance] empty railtiles", FKUtils.LOGLEVEL_ERRORS_AND_WARNINGS);
		return null;
	}
	else {
		if (distance > 1) { // ha pont 1, akkor nem kell tobbet hozzaadnunk, ezert mehet vissza ami van
			local counter = 0;
			local ujtiles = [];
			ujtiles.extend(railtiles);

			local alredychecked = [];
			while (counter < (distance - 1)) { // azaz milyen messze lopjon sint
				counter++;

				foreach (rtile in ujtiles) {
					local tiles = FKTile.GetNeighbours(rtile);
					alredychecked.append(rtile);

					foreach (a_tile_id, _ in tiles) {
						if (!FKUtils.ValueIsInArray(a_tile_id, railtiles)) {
							if (!GSTile.IsStationTile(a_tile_id) &&
								(GSTile.GetOwner(a_tile_id) == company_id) &&
								GSTile.HasTransportType(a_tile_id, GSTile.TRANSPORT_RAIL) &&
								this.IsNieighbourConnected(rtile, a_tile_id)) {
								railtiles.append(a_tile_id);
							}
						}
					}
				}

				ujtiles.clear();
				foreach (rtile in railtiles) {
					if (!FKUtils.ValueIsInArray(rtile, alredychecked) &&
						!FKUtils.ValueIsInArray(rtile, ujtiles)) {
							ujtiles.append(rtile);
						}
				}
			}
		}

		local tilelist = GSTileList();
		foreach (tile_id in railtiles) {
			tilelist.AddTile(tile_id);
		}

		return tilelist;
	}

	return null;
}

function FKRailTile::IsRailTileConnectedToTile(tile_id_1, tile_id_2) {
	// pf.forbid_90_deg
	assert(GSRail.IsRailTile(tile_id_1));
	assert(GSRail.IsRailTile(tile_id_2));

	assert(tile_id_1 != tile_id_2);

	local company_id = GSTile.GetOwner(tile_id_1);
	assert(company_id == GSTile.GetOwner(tile_id_2));

	local first_tiles = FKTile.GetNeighbours(tile_id_1);
	local railtiles = [];
	foreach (tile_id, _ in first_tiles) {
		local tiles = FKTile.GetNeighbours(tile_id);

		foreach (a_tile_id, _ in tiles) {
			if (FKUtils.ValueIsInArray(a_tile_id, railtiles) ||					// ha kozte van mar
			(GSTile.GetOwner(a_tile_id) != company_id) ||					// ha nem current company az owner
			!GSTile.HasTransportType(a_tile_id, GSTile.TRANSPORT_RAIL))		// es ha egyaltalan nem rail tilerol van-e szo
			continue;															// akkor folytatjuk a keresest

			if (this.IsNieighbourConnected(tile_id, a_tile_id)) {
				if (a_tile_id == tile_id_2)
					return true;

				railtiles.append(a_tile_id);
			}
		}
	}

	// ilyenkor elvileg megvan az osszes allomas koruli sajat vasuti sin
	if (railtiles.len() == 0) {
//		this.utils.Log("[FKRailTile::IsRailTileConnectedToTile] empty railtiles", FKUtils.LOGLEVEL_ERRORS_AND_WARNINGS);
		return false;
	}
	else {
		foreach (rtile in railtiles) {
			local tiles = FKTile.GetNeighbours(rtile);

			foreach (a_tile_id, _ in tiles) {
				if (!FKUtils.ValueIsInArray(a_tile_id, railtiles)) {
					if (!GSTile.IsStationTile(a_tile_id) &&
						(GSTile.GetOwner(a_tile_id) == company_id) &&
						GSTile.HasTransportType(a_tile_id, GSTile.TRANSPORT_RAIL) &&
						this.IsNieighbourConnected(rtile, a_tile_id)) {
						if (a_tile_id == tile_id_2)
							return true;

						railtiles.append(a_tile_id);
					}
				}
			}
		}
	}

	return false;
}

function FKRailTile::RemoveRailsOnTile(company_id, tile, demolish_anycase) {
	assert(GSMap.IsValidTile(tile));
	assert(GSRail.IsRailTile(tile));

	local companymode = GSCompanyMode(FKUtils.GetCompanyMode(company_id));

	local bitmap = GSRail.GetRailTracks(tile);
	if (bitmap == null || bitmap == GSRail.RAILTRACK_INVALID || bitmap == 0x00)
		return false;

	local removeAll = true;
	local demolished = false;
/*
	if (bitmap & GSRail.RAILTRACK_NE_SW) GSLog.Warning("van: RAILTRACK_NE_SW");
	if (bitmap & GSRail.RAILTRACK_NW_SE) GSLog.Warning("van: RAILTRACK_NW_SE");
	if (bitmap & GSRail.RAILTRACK_NW_NE) GSLog.Warning("van: RAILTRACK_NW_NE");
	if (bitmap & GSRail.RAILTRACK_SW_SE) GSLog.Warning("van: RAILTRACK_SW_SE");
	if (bitmap & GSRail.RAILTRACK_NW_SW) GSLog.Warning("van: RAILTRACK_NW_SW");
	if (bitmap & GSRail.RAILTRACK_NE_SE) GSLog.Warning("van: RAILTRACK_NE_SE");
*/

	/* egyszerubben alatta...
	if (GSRail.GetCurrentRailType() == GSRail.RAILTYPE_INVALID) {
		local railtypes = GSRailTypeList();
		GSRail.SetCurrentRailType(FKList.GetFirst(railtypes)); // tokmindegy melyiket allitjuk be, a lenyeg, hogy ne legyen invalid
	}
	 */
	if (!GSRail.IsRailTypeAvailable(GSRail.GetCurrentRailType()))
		GSRail.SetCurrentRailType(GSRail.GetRailType(tile));


	if ((bitmap & GSRail.RAILTRACK_NE_SW) && !GSRail.RemoveRailTrack(tile, GSRail.RAILTRACK_NE_SW)) {
		if (demolish_anycase && GSTile.DemolishTile(tile))
			demolished = true;
		else
			removeAll = false;
	}

	if (!demolished && (bitmap & GSRail.RAILTRACK_NW_SE) && !GSRail.RemoveRailTrack(tile, GSRail.RAILTRACK_NW_SE)) {
		if (demolish_anycase && GSTile.DemolishTile(tile))
			demolished = true;
		else
			removeAll = false;
	}

	if (!demolished && (bitmap & GSRail.RAILTRACK_NW_NE) && !GSRail.RemoveRailTrack(tile, GSRail.RAILTRACK_NW_NE)) {
		if (demolish_anycase && GSTile.DemolishTile(tile))
			demolished = true;
		else
			removeAll = false;
	}

	if (!demolished && (bitmap & GSRail.RAILTRACK_SW_SE) && !GSRail.RemoveRailTrack(tile, GSRail.RAILTRACK_SW_SE)) {
		if (demolish_anycase && GSTile.DemolishTile(tile))
			demolished = true;
		else
			removeAll = false;
	}

	if (!demolished && (bitmap & GSRail.RAILTRACK_NW_SW) && !GSRail.RemoveRailTrack(tile, GSRail.RAILTRACK_NW_SW)) {
		if (demolish_anycase && GSTile.DemolishTile(tile))
			demolished = true;
		else
			removeAll = false;
	}

	if (!demolished && (bitmap & GSRail.RAILTRACK_NE_SE) && !GSRail.RemoveRailTrack(tile, GSRail.RAILTRACK_NE_SE)) {
		if (demolish_anycase && GSTile.DemolishTile(tile))
			demolished = true;
		else
			removeAll = false;
	}

	return removeAll;
}

// - # Queue remove
function FKRailTile::QueueRemoveRailsOnTile(company_id, tile, demolish_anycase, otherinformations, subscriber) {
	if (this.RemoveRailsOnTile(company_id, tile, demolish_anycase)) {
		return true;
	}

	local queueinfo = {
		company_id = company_id,
		tile = tile,
		demolish_anycase = demolish_anycase,
		subscriber = subscriber,
		otherinformations = otherinformations,
	}

	this.queue.append(queueinfo);

	return false; // nem sikerult most a torles, ezert hozzaadtuk a queue-hez
}

function FKRailTile::QueueUpdate() {
	if (this.queue.len() == 0)
		return;

	for (local i = 0; i < this.queue.len(); i++) {
		local queueinfo = this.queue[i];
		if (this.RemoveRailsOnTile(queueinfo.company_id, queueinfo.tile, queueinfo.demolish_anycase)) {
			this.queue.remove(i);
			i--;
			queueinfo.subscriber.RailTileQueueSucces(queueinfo.company_id, queueinfo.otherinformations);
		}
	}
}
