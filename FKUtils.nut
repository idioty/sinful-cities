
class FKUtils {
	static LOGLEVEL_ERRORS = 0;
	static LOGLEVEL_ERRORS_AND_WARNINGS = 1;
	static LOGLEVEL_ALL = 2;
	loglevel = 0;			// kuldjunk-e minden logot, fentebb az ertekek

	constructor (llevel) {
		loglevel = llevel;
	}
	
	static function ValueIsInArray(value, list);
	static function GetCompanyMode(company_id);
}


////////////////////////////////
// - #  instance methodes     //
////////////////////////////////

function FKUtils::Log(message, llevel) {
	if (llevel <= loglevel)
		return;

	switch (llevel) {
		case FKUtils.LOGLEVEL_ERRORS:
			GSLog.Error(FKDate.GetCurrentDateLogString() + message);
			break;
		case FKUtils.LOGLEVEL_ERRORS_AND_WARNINGS:
			GSLog.Warning(FKDate.GetCurrentDateLogString() + message);
			break;
		case FKUtils.LOGLEVEL_ALL:
		default:
			GSLog.Info(FKDate.GetCurrentDateLogString() + message);
			break;
	}
}


////////////////////////////////
// - #  class methodes        //
////////////////////////////////

function FKUtils::ValueIsInArray(value, list) {
	foreach (avalue in list) {
		if (avalue == value) {
			return true;
		}
	}

	return false;
}

function FKUtils::GetCompanyMode(company_id) {
	if (company_id != null && company_id != GSCompany.COMPANY_INVALID) {
		for (local company_id_t = GSCompany.COMPANY_FIRST; company_id_t <= GSCompany.COMPANY_LAST; company_id_t++) {
			local a_company_id = GSCompany.ResolveCompanyID(company_id_t);
			if (a_company_id == GSCompany.COMPANY_INVALID) continue;
			if (a_company_id == company_id) {
				return company_id_t;
				break;
			}
		}
	}

//	this.utils.Log("[FKUtils::GetCompanyMode] Invalid (bankrupt???) company?");
	return null;
}
