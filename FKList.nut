
class FKList {
	static function GetFirst(list);
	static function GetLast(list);
	static function GetIndexAt(list, index);
}

function FKList::GetFirst(list) {
	if (list.IsEmpty())
		return null;

	foreach (index, _ in list) {
		return index;
	}

	return null;
}

function FKList::GetLast(list) {
	if (list.IsEmpty())
		return null;

	local ar = [];
	foreach (index, _ in list) {
		ar.append(index);
	}

	return ar.top();
}

function FKList::GetIndexAt(list, index) {
	if (list.IsEmpty())
		return null;

	local ar = [];
	foreach (aindex, _ in list) {
		ar.append(aindex);
	}

	if ((ar.len() - 1) < index)
		return null;

	return ar[index];
}
