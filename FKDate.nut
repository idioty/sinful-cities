
class FKDate {
	startyear = 0;

	constructor () {
	}

	static function GetCurrentDay();
	static function GetCurrentMonth();
	static function GetPreviousMonthDayCount();
	static function GetCurrentDaysCount();
	static function GetDayCountOfMonth(month, year);
	static function GetCurrentDateLogString();
}


////////////////////////////////
// - #  instance methodes     //
////////////////////////////////

function FKDate::GetMonthsCountFromStarted () {
	local year = GSDate.GetYear(GSDate.GetCurrentDate());
	return ((year - this.startyear) * 12) + GSDate.GetMonth(GSDate.GetCurrentDate()) - 1; // azert a -1, hogy 0 legyen a kezdohonap a lista miatt
}


////////////////////////////////
// - #  class methodes        //
////////////////////////////////

function FKDate::GetCurrentDay() {
	return GSDate.GetDayOfMonth(GSDate.GetCurrentDate());
}

function FKDate::GetCurrentMonth() {
	return GSDate.GetMonth(GSDate.GetCurrentDate());
}

function FKDate::GetPreviousMonthDayCount() {
	local date = GSDate.GetCurrentDate();
	local month = GSDate.GetMonth(date);
	local year = GSDate.GetYear(date);

	if (month == 1) {
		month = 12;
		year--;
	}
	else
		month--;

	return FKDate.GetDayCountOfMonth(month, year);
}

function FKDate::GetCurrentDaysCount() {
	local date = GSDate.GetCurrentDate();
	local month = GSDate.GetMonth(date);
	local year = GSDate.GetYear(date);

	return FKDate.GetDayCountOfMonth(month, year);
}

function FKDate::GetDayCountOfMonth(month, year) {
	local szokoev = false;
	if ((year % 4) == 0) {
		if ((year % 100) > 0) {
			szokoev = true;
		}
		else {
			// szazadfordulok csak akkor szokoevek, ha 400-al oszthatoak
			if ((year % 400) == 0)
				szokoev = true;
		}
	}

	switch (month) {
		case 1: // januar
			return 31;
			break;
		case 2: // februar
			if (szokoev)
				return 29;
			else
				return 28;
			break;
		case 3: // marcius
			return 31;
			break;
		case 4: // aprilis
			return 30;
			break;
		case 5: // majus
			return 31;
			break;
		case 6: // junius
			return 30;
			break;
		case 7: // julius
			return 31;
			break;
		case 8: // augusztus
			return 31;
			break;
		case 9: // szeptember
			return 30;
			break;
		case 10: // oktober
			return 31;
			break;
		case 11: // november
			return 30;
			break;
		case 12: // december
			return 31;
			break;
		default:
			return 30;
			break;
	}

	return 30;
}

function FKDate::GetCurrentDateLogString() {
	local time = GSDate.GetCurrentDate();
	local year = GSDate.GetYear(time);
	local month = GSDate.GetMonth(time);
	local day = GSDate.GetDayOfMonth(time);

	return ("[" + year + "-" + ((month < 10) ? ("0" + month) : month) + "-" + ((day < 10) ? ("0" + day) : day) + "]");
}
