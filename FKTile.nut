
class FKTile {
	static function GetNeighbours(tile_id);
	static function DemolishTile(company_id, tile);
}

function FKTile::GetNeighbours(tile_id) {
	assert(GSMap.IsValidTile(tile_id));

	local tilelist = GSTileList();

	local x = GSMap.GetTileX(tile_id);
	local y = GSMap.GetTileY(tile_id);

	if (x > 0)
		tilelist.AddTile(GSMap.GetTileIndex(x - 1, y));

	if (y > 0)
		tilelist.AddTile(GSMap.GetTileIndex(x, y - 1));

	if (x < (GSMap.GetMapSizeX() - 1))
		tilelist.AddTile(GSMap.GetTileIndex(x + 1, y));

	if (y < (GSMap.GetMapSizeY() - 1))
		tilelist.AddTile(GSMap.GetTileIndex(x, y + 1));

	return tilelist;
}

function FKTile::DemolishTile(company_id, tile) {
	assert(GSMap.IsValidTile(tile));
	local companymode = GSCompanyMode(FKUtils.GetCompanyMode(company_id));
	return GSTile.DemolishTile(tile);
}
