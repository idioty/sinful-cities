
class FKTown {
	static function GetValueOfTownRating(rating);
	static function GetTownRatingFromValue(value);
}


////////////////////////////////
// - #  class methodes        //
////////////////////////////////

// # - Rating functions
function FKTown::GetValueOfTownRating(rating) {
	switch (rating) {
		case GSTown.TOWN_RATING_NONE:
			return 0;
			break;
		case GSTown.TOWN_RATING_APPALLING:
			return 1;
			break;
		case GSTown.TOWN_RATING_VERY_POOR:
			return 2;
			break;
		case GSTown.TOWN_RATING_POOR:
			return 3;
			break;
		case GSTown.TOWN_RATING_MEDIOCRE:
			return 4;
			break;
		case GSTown.TOWN_RATING_GOOD:
			return 5;
			break;
		case GSTown.TOWN_RATING_VERY_GOOD:
			return 6;
			break;
		case GSTown.TOWN_RATING_EXCELLENT:
			return 7;
			break;
		case GSTown.TOWN_RATING_OUTSTANDING:
			return 8;
			break;
		case GSTown.TOWN_RATING_INVALID:
			return 0;
			break;
	}

	return 0;
}

function FKTown::GetTownRatingFromValue(value) {
	local kerekitettValue = value.tointeger();
	switch (kerekitettValue) {
		case 0:
			return GSTown.TOWN_RATING_NONE;
			break;
		case 1:
			return GSTown.TOWN_RATING_APPALLING;
			break;
		case 2:
			return GSTown.TOWN_RATING_VERY_POOR;
			break;
		case 3:
			return GSTown.TOWN_RATING_POOR;
			break;
		case 4:
			return GSTown.TOWN_RATING_MEDIOCRE;
			break;
		case 5:
			return GSTown.TOWN_RATING_GOOD;
			break;
		case 6:
			return GSTown.TOWN_RATING_VERY_GOOD;
			break;
		case 7:
			return GSTown.TOWN_RATING_EXCELLENT;
			break;
		case 8:
			return GSTown.TOWN_RATING_OUTSTANDING;
			break;
	}

	return GSTown.TOWN_RATING_NONE;
}