
class Unemployment extends GSInfo {
	function GetAuthor()		{ return "idioty"; }
	function GetName()			{ return "Unemployment"; }
	function GetDescription() 	{ return "Unemployment! :)"; }
	function GetVersion()		{ return 1; }
	function GetDate()			{ return "2013-12-31"; }
	function CreateInstance()	{ return "Unemployment"; }
	function GetShortName()		{ return "UELM"; }
	function GetAPIVersion()	{ return "1.2"; }
	function GetUrl()			{ return ""; }
	function GetSettings() {
		
		/*
		CONFIG_NONE,          ez az alap
		CONFIG_RANDOM,        veletlenszeruen valaszt a min es maxbol
		CONFIG_BOOLEAN,       az ertek bool tipusu: 0 false, 1 true
		CONFIG_INGAME,        jatek alatt is megvaltoztathato ez a parameter
		CONFIG_DEVELOPER      csak akkor jelenik meg, ha a development tools aktiv
		*/

		AddSetting({name = "enabled", description									= "Enable", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, flags = CONFIG_BOOLEAN | CONFIG_INGAME});


		/*
		// valami meg valami nos vazze
		AddSetting({name = "startAfterMinute", description									= "Játék hány perc múlva induljon", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 43200, flags = CONFIG_INGAME});
		AddSetting({name = "startGameImmediately", description								= "Játék indítása azonnal!", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = CONFIG_BOOLEAN | CONFIG_INGAME});
		AddSetting({name = "timeZone", description										= "Időzóna", min_value = 0, max_value = 24, easy_value = 12, medium_value = 12, hard_value = 12, custom_value = 12, flags = CONFIG_INGAME});

		// ures sor
		AddSetting({name = "info0000", description										= "", min_value = 0, max_value = 0, easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0});

		AddSetting({name = "gameTime", description										= "        Hány hónapig tartson a játék", easy_value = 0, medium_value = 120, hard_value = 60, custom_value = 0, min_value = 0, max_value = 12000, flags = 0});

		AddSetting({name = "awardAirVehicles", description									= "                Repülőgép lehet jutalom", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});

		AddLabels("info0000", {_0 = ""});
		AddLabels("enableStoryCupUpdates", {_0 = "Ne jelenjen meg", _1 = "Pontok nélkül jelenejen meg", _2 = "Pontokkal együtt jelenjen meg"});
		*/
	}
}

RegisterGS(Unemployment());
